# Different Caching Approaches To Improve Performance

## Introduction

The improvement of performance in a system really helps a user or an organization or even things dependent on that system in many aspects. It’s important to develop a product which is highly scalable and provides better performance. There are many ways through which better scalability and performances can be achieved. Caching is one of the highly preferred ways of improving the performance and user experience. The way it does its job depends upon the caching approach.

There are different caching approaches for different use cases. There are different caching approaches for different use cases. Before choosing any approach we need to make sure or analyse certain things like-

- In which module there’s a requirement of low latency, fast response rate and higher throughput?
- Can data inconsistency occur upto some extent if we use caching?
- What type of data would we like to store in that cache?
- Does the system perform read, write operations heavily or a combination of both?

## Approaches

**Read Through/ Lazy Loading**:
If there is a need of data then load only the required data into the cache. If the application requires something then first search in the cache, if the data is present in the cache then return that data else retrieve that data from the data source and load that data into cache and then return that.

Advantages:

- It follows on demand loading which reduces memory usage and time consumption and optimizes content delivery.

Disadvantages:

- Extra code needs to be written, which sometimes makes it complicated.

**Write Through**: The application is simultaneously inserted or updated a piece of data in the cache and underlying data source as well. This a simple and reliable process where both operations can be done in a single transaction.

Advantages:

- It overcomes the data inconsistency issue and helps in recovering data in case of system failure.

Disadvantages:

- Since the purpose of cache is to make less memory access, the writing operation to the data source as well as to the cache is little inefficient.

**Write Back Caching**: In this approach, the data is first updated in the cache and after a certain interval the data is updated to the data source.

Advantages:

- It improves performance since the data is first written to the caching service and it doesn’t need to wait till the completion of writing data to the data source.

Disadvantages:

- There is be a possibility of data loss and unavailability if cache fails before the data is written to the data store.

**Refresh Ahead Caching**: The refresh ahead technique is used to refresh cached data before it expires. The process ensures that the end-users never have to suffer the delay of the refresh.

Advantages:

- It’s useful when large number of users are using the same cache keys. Since the data is refreshed periodically & frequently, staleness of data is not a permanent problem.
  Reduced latency than other technique like Read Through cache.

Disadvantages:

- It can be a little hard to implement since cache service takes extra pressure to refresh all the keys as and when they are accessed. But incase of a read heavy environment, it is very efficient.

## References

[www.medium.com](https://medium.com/datadriveninvestor/all-things-caching-use-cases-benefits-strategies-choosing-a-caching-technology-exploring-fa6c1f2e93aa)

[www.geeksforgeeks.org](https://www.geeksforgeeks.org/write-through-and-write-back-in-cache/)
